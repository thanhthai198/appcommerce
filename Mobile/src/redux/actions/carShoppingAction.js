import {
  ADD_TO_CART,
  REMOVE_FROM_CART,
  SUB_QUANTITY,
  ADD_QUANTITY,
  EMPTY_CART,
} from '@constants/cartShoppingType';

const addToCart = id => ({
  type: ADD_TO_CART,
  data: id,
});
const removeFromCart = id => ({
  type: REMOVE_FROM_CART,
  data: id,
});
const subtractQuantity = id => ({
  type: SUB_QUANTITY,
  data: id,
});
const addQuantity = id => ({
  type: ADD_QUANTITY,
  data: id,
});
const emptyCart = () => ({
  type: EMPTY_CART,
  data: id,
});

export {addToCart, emptyCart, addQuantity, removeFromCart, subtractQuantity};
