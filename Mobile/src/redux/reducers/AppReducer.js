import {createReducer, updateObject} from './ReducerUtils';
import {REDUX_TYPES} from '../../constants/reduxTypes';

const initialState = {
  loading: false,
  appPopUps: [],
};

function showAppPopup(state, payload) {
  if (payload.data) {
    state.appPopUps.push(payload.data);
  }
  const newState = updateObject(state, {
    appPopUps: [...state.appPopUps],
  });
  return newState;
}

function dismissAppPopup(state) {
  if (state.appPopUps && state.appPopUps.length > 0) {
    state.appPopUps.shift();
  }
  const newState = updateObject(state, {
    appPopUps: [...state.appPopUps],
  });
  return newState;
}

function showAppLoading(state, action) {
  const newState = updateObject(state, {
    loading: action.data,
  });
  return newState;
}

const handles = {};
handles[REDUX_TYPES.SHOW_APP_LOADING] = showAppLoading;
handles[REDUX_TYPES.SHOW_APP_POPUP] = showAppPopup;
handles[REDUX_TYPES.DISMISS_APP_POPUP] = dismissAppPopup;

const AppReducer = createReducer(initialState, handles);

export default AppReducer;
