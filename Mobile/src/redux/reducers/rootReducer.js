import {combineReducers} from 'redux';
import AppReducer from './AppReducer';
// import AuthReducer from './AuthReducer';
// import UserReducer from './UserReducer';
import CartShopping from './cartShoppingReducer';

const RootReducer = combineReducers({
  app: AppReducer,
  cart: CartShopping,
  //   auth: AuthReducer,
  //   user: UserReducer,
});

export default RootReducer;
