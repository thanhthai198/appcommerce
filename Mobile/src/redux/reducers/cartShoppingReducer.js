import {
  ADD_TO_CART,
  REMOVE_FROM_CART,
  SUB_QUANTITY,
  ADD_QUANTITY,
  EMPTY_CART,
} from '@constants/cartShoppingType';
import {createReducer, updateObject} from './ReducerUtils';

const initialState = {
  products: [],
};

function addToCart(state, payload) {
  if (payload.data) {
    state.products.push(payload.data);
  }
  const newState = updateObject(state, {
    products: [...state.products],
  });
  return newState;
}

function removeFromCart(state) {
  if (state.products && state.products.length > 0) {
    state.products.shift();
  }
  const newState = updateObject(state, {
    products: [...state.products],
  });
  return newState;
}

const handles = {};

handles[ADD_TO_CART] = addToCart;
handles[REMOVE_FROM_CART] = removeFromCart;

const shoppingReducer = createReducer(initialState, handles);

export default shoppingReducer;
