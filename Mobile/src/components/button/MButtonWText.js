import {MButton} from './MButton';
import {setFontSize, setHorizontal, setVertical} from '../../utils';
import colors from '../../theme/colors';
import {MText} from '../MText';
import React from 'react';
import PropTypes from 'prop-types';

MButtonWText.propTypes = {
  style: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  textStyle: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  onPress: PropTypes.func,
  disabled: PropTypes.bool,
  title: PropTypes.string,
};

MButtonWText.defaultProps = {
  disabled: undefined,
};

export function MButtonWText(props) {
  const {style, disabled, title, onPress, textStyle} = props;

  return (
    <MButton
      disabled={disabled}
      style={[
        {
          height: setVertical(50),
          width: setHorizontal(330),
          backgroundColor: disabled
            ? colors.textColors.primary
            : colors.textColors.addtoCart,
          borderRadius: setVertical(25),
          justifyContent: 'center',
          alignItems: 'center',
        },
        style,
      ]}
      onPress={onPress}>
      <MText
        className={'headerText'}
        style={[
          {
            fontWeight: 'bold',
            color: 'white',
            fontSize: setFontSize(16),
          },
          textStyle,
        ]}
        text={title}
      />
    </MButton>
  );
}
