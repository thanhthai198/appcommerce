import {MButton} from './MButton';
import {Image} from 'react-native';
import React from 'react';
import PropTypes from 'prop-types';
import {RESIZE_MODE} from '../MImage';
import {MText} from '../MText';
import colors from '@theme/colors';
import {
  setVertical,
  setFontSize,
  setHorizontal,
  screenW,
  screenH,
} from '@utils/index';
import IconBtn from 'react-native-vector-icons/Ionicons';

MButtonAddToCart.propTypes = {
  style: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  textStyles: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  containerStyle: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  onPress: PropTypes.func,
  disabled: PropTypes.bool,
  source: PropTypes.number,
  uri: PropTypes.string,
  resizeMode: PropTypes.string,
  title: PropTypes.string,
  iconColor: PropTypes.string,
  iconName: PropTypes.string,
  iconSize: PropTypes.number,
  isBackgroundColor: PropTypes.bool,
};

MButtonAddToCart.defaultProps = {
  disabled: false,
  isBackgroundColor: false,
};

export function MButtonAddToCart(props) {
  const {
    style,
    disabled,
    source,
    uri,
    onPress,
    containerStyle,
    title,
    textStyles,
    iconName,
    iconSize,
    iconColor,
    isBackgroundColor,
  } = props;

  return (
    <MButton
      disabled={disabled}
      onPress={onPress}
      style={[
        containerStyle,
        {
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'row',
          width: '100%',
          height: 30,
          backgroundColor: isBackgroundColor
            ? colors.backgroundColorBtnAddToCart.secondary
            : colors.backgroundColorBtnAddToCart.addToCart,
          marginVertical: setVertical(6.5),
          borderBottomRightRadius: 5,
          borderBottomLeftRadius: 5,
        },
      ]}>
      {iconName ? (
        <IconBtn name={iconName} size={iconSize} color={iconColor} />
      ) : (
        <Image
          source={source}
          uri={uri}
          style={[style]}
          resizeMode={RESIZE_MODE.CONTAIN}
        />
      )}
      <MText
        style={[
          textStyles,
          {
            fontSize: setFontSize(12),
            fontWeight: '600',
            color: colors.textColors.secondary,
            marginHorizontal: setHorizontal(10),
          },
        ]}
        text={title}
      />
    </MButton>
  );
}
