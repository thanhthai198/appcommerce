import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import PropTypes from 'prop-types';

MButton.propTypes = {
  style: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  onPress: PropTypes.func,
  disabled: PropTypes.bool,
};

MButton.defaultProps = {
  disabled: undefined,
};

export function MButton(props) {
  const {style, onPress, disabled} = props;
  return (
    <TouchableOpacity
      {...props}
      style={[styles.view, style]}
      onPress={onPress}
      activeOpacity={onPress ? 0.2 : 1}
      disabled={disabled !== undefined ? disabled : !onPress}
    />
  );
}

const styles = StyleSheet.create({
  view: {},
});
