import {MButton} from './MButton';
import {Image} from 'react-native';
import React from 'react';
import PropTypes from 'prop-types';
import {RESIZE_MODE} from '../MImage';
import IconBtn from 'react-native-vector-icons/Ionicons';

MButtonIcon.propTypes = {
  style: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  containerStyle: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  onPress: PropTypes.func,
  disabled: PropTypes.bool,
  source: PropTypes.number,
  uri: PropTypes.string,
  resizeMode: PropTypes.string,
  iconColor: PropTypes.string,
  iconName: PropTypes.string,
  iconSize: PropTypes.number,
};

MButtonIcon.defaultProps = {
  disabled: false,
};

export function MButtonIcon(props) {
  const {
    style,
    disabled,
    source,
    uri,
    onPress,
    containerStyle,
    iconName,
    iconSize,
    iconColor,
  } = props;

  return (
    <MButton
      disabled={disabled}
      onPress={onPress}
      style={[
        containerStyle,
        {
          alignItems: 'center',
          justifyContent: 'center',
        },
      ]}>
      {iconName ? (
        <IconBtn name={iconName} size={iconSize} color={iconColor} />
      ) : (
        <Image
          source={source}
          uri={uri}
          style={[style]}
          resizeMode={RESIZE_MODE.CONTAIN}
        />
      )}
    </MButton>
  );
}
