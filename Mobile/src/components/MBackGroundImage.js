import React from 'react';
import {StyleSheet} from 'react-native';
// import FastImage from 'react-native-fast-image';
import PropTypes from 'prop-types';
import MImage from './MImage';
import {screenH, screenW} from '../utils';

MBackgroundImage.propTypes = {
  style: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  source: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};
MBackgroundImage.defaultProps = {
  source: '',
};

function MBackgroundImage(props) {
  const {style} = props;
  return (
    <MImage
      className={'view-absolute flex-1'}
      {...props}
      style={[styles.image, style]}
    />
  );
}

const styles = StyleSheet.create({
  image: {
    top: 0,
    left: 0,
    width: screenW,
    height: screenH,
    position: 'absolute',
  },
});

export default MBackgroundImage;
