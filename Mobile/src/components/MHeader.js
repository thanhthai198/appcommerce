import React from 'react';
import {Image} from 'react-native';
import PropTypes from 'prop-types';
import {setFontSize, setHorizontal, setVertical, screenW} from '@utils/index';
import colors from '@theme/colors';
import {RESIZE_MODE} from '@components/MImage';
import {MView, MText, MButton} from '@components/index';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import IconF from 'react-native-vector-icons/FontAwesome';
import IconA from 'react-native-vector-icons/AntDesign';

MHeader.propTypes = {
  style: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  disabled: PropTypes.bool,
  iconLeft: PropTypes.any,
  uri: PropTypes.string,
  resizeMode: PropTypes.string,
  leftIconStyle: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  rightIconStyle: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  textStyle: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onPress: PropTypes.func,
  hasBorderBottom: PropTypes.bool,
  borderBottomColor: PropTypes.string,
  rightIcon: PropTypes.any,
  onPressRightButton: PropTypes.func,
  isBackgroundColor: PropTypes.bool,
  textLengthCart: PropTypes.string,
  deleteIcon: PropTypes.any,
  onPressDelete: PropTypes.func,
};
MHeader.defaultProps = {
  disabled: undefined,
  isAbsolute: undefined,
  isBackgroundColor: undefined,
  hasBorderBottom: true,
};

export function MHeader(props) {
  const {
    style,
    uri,
    iconLeft,
    disabled,
    title,
    textStyle,
    isAbsolute,
    isBackgroundColor,
    onPress,
    leftIconStyle,
    hasBorderBottom,
    borderBottomColor,
    rightIcon,
    rightIconStyle,
    onPressRightButton,
    textLengthCart,
    deleteIcon,
    onPressDelete,
  } = props;
  return (
    <MView
      style={[
        {
          position: isAbsolute ? 'absolute' : null,
          backgroundColor: isBackgroundColor
            ? colors.backgroundColorHeader.secondary
            : colors.backgroundColorHeader.primary,
          height: setVertical(88),
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          width: '100%',
          borderBottomWidth: hasBorderBottom ? setVertical(0.6) : 0,
          borderBottomColor: borderBottomColor || colors.borderColors.secondary,
          paddingTop: setVertical(ifIphoneX(34, 0)),
        },
        style,
      ]}>
      <MButton
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          paddingLeft: setHorizontal(20),
          width: setHorizontal(62),
          height: '100%',
          padding: setHorizontal(8),
        }}
        onPress={onPress}
        disabled={disabled}>
        {iconLeft && (
          <IconF
            style={leftIconStyle}
            name={iconLeft}
            size={25}
            color={colors.iconColor.primary}
          />
        )}
      </MButton>
      <MText
        className={'headerText'}
        style={[
          {
            fontWeight: 'bold',
            fontSize: setFontSize(17),
            textAlign: 'center',
            width: screenW - setHorizontal(62 * 2),
            color: colors.textColors.secondary,
          },
          textStyle,
        ]}
        text={title}
        numberOfLines={2}
      />
      <MButton
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginRight: setHorizontal(20),
          width: setHorizontal(42),
          height: '100%',
        }}
        onPress={onPressRightButton}
        disabled={disabled}>
        {rightIcon && (
          <>
            <MView
              style={{
                position: 'absolute',
                minHeight: setVertical(28),
                minWidth: setVertical(28),
                borderRadius: setVertical(15),
                bottom: setVertical(22),
                right: setVertical(15),
                backgroundColor: 'rgba(95,197,123,0.8)',
                alignItems: 'center',
                justifyContent: 'center',
                zIndex: 2000,
              }}>
              <MText
                style={{
                  color: 'white',
                  fontSize: setFontSize(13),
                  fontWeight: '500',
                  maxWidth: setVertical(50),
                }}
                numberOfLines={1}
                text={textLengthCart}
              />
            </MView>
            <IconF
              style={rightIconStyle}
              name={rightIcon}
              size={25}
              color={colors.iconColor.primary}
            />
          </>
        )}
        {deleteIcon && (
          <>
            <MButton onPress={() => onPressDelete}>
              <IconA
                name={'delete'}
                size={23}
                color={colors.iconColor.primary}
              />
            </MButton>
          </>
        )}
      </MButton>
    </MView>
  );
}
