import React from 'react';
import {StyleSheet, Image} from 'react-native';
import PropTypes from 'prop-types';
import FastImage from 'react-native-fast-image';

MImage.propTypes = {
  style: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
  uri: PropTypes.string,
  source: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  resizeMode: PropTypes.any,
};

MImage.defaultProps = {
  resizeMode: FastImage.resizeMode.cover,
  uri: '',
};

export const RESIZE_MODE = {
  COVER: FastImage.resizeMode.cover,
  CONTAIN: FastImage.resizeMode.contain,
};

function MImage(props) {
  const {style, uri, source, resizeMode} = props;
  if (uri) {
    return (
      <FastImage
        {...props}
        style={[styles.image, style]}
        source={uri ? {uri: uri} : source}
        resizeMode={resizeMode}
      />
    );
  }

  return (
    <Image
      {...props}
      style={[styles.image, style]}
      source={source}
      resizeMode={resizeMode}
    />
  );
}

const styles = StyleSheet.create({
  image: {},
});

export default MImage;
