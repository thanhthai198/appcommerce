import './services/TranslationService';

import React from 'react';
import AppContainer from './AppContainer';
import {NavigationContainer} from '@react-navigation/native';
import {navigationRef} from './services/NavigationService';
import {ThemeProvider} from 'react-native-picasso';
import {theme} from './theme';
import {PersistGate} from 'redux-persist/integration/react';
import {persistor, store} from './redux/store';
import {Provider} from 'react-redux';
import {LogBox} from 'react-native';

const App = () => {
  LogBox.ignoreLogs([
    "[react-native-gesture-handler] Seems like you're using an old API with gesture components, check out new Gestures system!",
  ]);
  return (
    <>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <ThemeProvider theme={theme}>
            <NavigationContainer ref={navigationRef}>
              <AppContainer />
            </NavigationContainer>
          </ThemeProvider>
        </PersistGate>
      </Provider>
    </>
  );
};

export default App;
