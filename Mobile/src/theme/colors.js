export default {
  colors: {
    primary: '#FFFFFF',
    secondary: '#000000',
    colorBtnCounters: '#039447',
  },
  textColors: {
    primary: '#363636',
    secondary: '#FFFFFF',
    colorPrice: '#C10230',
    addtoCart: '#0693E3',
    textLabel: '#808080',
  },
  borderColors: {
    primary: '#DCDCDC',
    secondary: '#E7E7E7',
    iconActive: '#008B02',
  },
  bottomTab: {
    primary: '#808080',
    secondary: '#C10230',
  },
  backgroundColorHeader: {
    primary: '#4A90E2',
    secondary: '#FFFFFF',
  },
  iconColor: {
    primary: '#FFFFFF',
    secondary: '#000000',
    cart: '#D0021B',
    iconClose: 'rgba(0,0,0,0.7)',
  },
  backgroundColorBtnAddToCart: {
    primary: '#FFFFFF',
    secondary: '#D0021B',
    addToCart: '#039447',
  },
  placeholderTextColor: '#AAAAAA',
};
