import i18next from 'i18next';
import {initReactI18next} from 'react-i18next';
import vi from '../locales/vi.json';

const languageDetector = {
  type: 'languageDetector',
  async: true,
  detect: cb => cb('vi'),
  init: () => {},
  cacheUserLanguage: () => {},
};

i18next.use(languageDetector).use(initReactI18next).init({
  compatibilityJSON: 'v3',
  fallbackLng: 'vi',
  debug: false,
  resources: {vi},
  nsSeparator: false,
});
