import React from 'react';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import {SCREEN_NAMES} from '../constants/screenNames';
import HomeNavigator from './HomeNavigator';
import CartShopping from '@screens/CartShopping';

import {useSelector} from 'react-redux';

const RootNavigation = props => {
  const Stack = createStackNavigator();
  const {user} = props;

  const UserScreen = () => {
    return (
      <>
        <Stack.Screen
          name={SCREEN_NAMES.HOME_NAVIGATOR}
          component={HomeNavigator}
        />
        <Stack.Screen
          name={SCREEN_NAMES.CART_SHOPPING}
          component={CartShopping}
        />
      </>
    );
  };

  return (
    <Stack.Navigator
      initialRouteName={SCREEN_NAMES.HOME_SCREEN}
      screenOptions={{
        headerShown: false,
      }}>
      {UserScreen()}
    </Stack.Navigator>
  );
};

export default RootNavigation;
