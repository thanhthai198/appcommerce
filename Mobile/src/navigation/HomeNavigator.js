import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Menu from '@screens/Menu';
import Store from '@screens/Store';
import Home from '@screens/Home';
import BarCode from '@screens/BarCode';
import {setFontSize, setVertical} from '../utils';
import Account from '@screens/Account';
import {MButton, MText, MView} from '../components';
import {navigate} from '../services/NavigationService';
import {SCREEN_NAMES} from '../constants/screenNames';
import colors from '../theme/colors';
import IconF from 'react-native-vector-icons/dist/FontAwesome5';
import IconS from 'react-native-vector-icons/dist/SimpleLineIcons';
import IconM from 'react-native-vector-icons/dist/MaterialCommunityIcons';

const HomeNavigator = () => {
  const Tab = createBottomTabNavigator();

  return (
    <Tab.Navigator
      screenOptions={{
        showLabel: true,
        lazy: true,
        keyboardHidesTabBar: true,
        headerShown: false,
        activeTintColor: colors.colors.secondary,
        inactiveTintColor: colors.textColors.color808080,
      }}>
      <Tab.Screen
        name={SCREEN_NAMES.HOME_SCREEN}
        component={Home}
        options={{
          tabBarLabel: ({color, focused}) => {
            return (
              <MText
                text={'bottomTab.Home'}
                style={{
                  fontSize: setFontSize(11),
                  color: focused
                    ? colors.colors.secondary
                    : colors.textColors.color808080,
                }}
              />
            );
          },
          tabBarIcon: ({color, focused}) => (
            <IconF
              name="home"
              size={25}
              color={
                focused ? colors.bottomTab.secondary : colors.bottomTab.primary
              }
            />
          ),
        }}
      />
      <Tab.Screen
        name={SCREEN_NAMES.MENU_SCREEN}
        component={Menu}
        options={{
          tabBarLabel: ({color, focused}) => {
            return (
              <MText
                text={'bottomTab.Menu'}
                style={{
                  fontSize: setFontSize(11),
                  color: focused
                    ? colors.colors.secondary
                    : colors.textColors.color808080,
                }}
              />
            );
          },
          tabBarIcon: ({color, focused}) => (
            <IconF
              name="th-large"
              size={25}
              color={
                focused ? colors.bottomTab.secondary : colors.bottomTab.primary
              }
            />
          ),
        }}
      />
      <Tab.Screen
        name={SCREEN_NAMES.BAR_SCREEN}
        component={BarCode}
        options={{
          tabBarLabel: ({color, focused}) => {
            return (
              <MText
                text={'bottomTab.BarCode'}
                style={{
                  fontSize: setFontSize(11),
                  color: focused
                    ? colors.colors.secondary
                    : colors.textColors.color808080,
                }}
              />
            );
          },
          tabBarIcon: ({color, focused}) => (
            <IconM
              name="barcode-scan"
              size={25}
              color={
                focused ? colors.bottomTab.secondary : colors.bottomTab.primary
              }
            />
          ),
        }}
      />
      <Tab.Screen
        name={SCREEN_NAMES.STORE_SCREEN}
        component={Store}
        options={{
          tabBarLabel: ({color, focused}) => {
            return (
              <MText
                text={'bottomTab.Store'}
                style={{
                  fontSize: setFontSize(11),
                  color: focused
                    ? colors.colors.secondary
                    : colors.textColors.color808080,
                }}
              />
            );
          },
          tabBarIcon: ({color, focused}) => (
            <IconF
              name="store-alt"
              size={25}
              color={
                focused ? colors.bottomTab.secondary : colors.bottomTab.primary
              }
            />
          ),
        }}
      />
      <Tab.Screen
        name={SCREEN_NAMES.ACCOUNT_SCREEN}
        component={Account}
        options={{
          tabBarLabel: ({color, focused}) => {
            return (
              <MText
                text={'bottomTab.Settings'}
                style={{
                  fontSize: setFontSize(11),
                  color: focused
                    ? colors.colors.secondary
                    : colors.textColors.color808080,
                }}
              />
            );
          },
          tabBarIcon: ({color, focused}) => (
            <IconS
              name="settings"
              size={25}
              color={
                focused ? colors.bottomTab.secondary : colors.bottomTab.primary
              }
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default HomeNavigator;
