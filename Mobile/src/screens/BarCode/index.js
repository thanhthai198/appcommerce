import React from 'react';
import {MText, MView} from '@components/index';
import styles from './styles';

const BarScreen = () => {
  return (
    <MView style={styles.container}>
      <MText text={'common.ads1'} />
    </MView>
  );
};

export default BarScreen;
