import React, {useState} from 'react';
import {
  MText,
  MView,
  MScrollView,
  MHeader,
  MTextInput,
  MButton,
  MFlatList,
  MButtonIcon,
} from '@components/index';
import styles from './styles';
import {useSelector, useDispatch} from 'react-redux';
import {goBack} from '@services/NavigationService';
import IconF from 'react-native-vector-icons/FontAwesome5';
import IconFA from 'react-native-vector-icons/FontAwesome';
import IconM from 'react-native-vector-icons/MaterialIcons';
import colors from '@theme/colors';
import RadioButtonRN from 'radio-buttons-react-native';
import {screenW, setHorizontal, setFontSize, setVertical} from '@utils/index';
import MImage from '@components/MImage';
import {
  addToCart,
  removeFromCart,
  subtractQuantity,
  addQuantity,
  emptyCart,
} from '@redux/actions/carShoppingAction';

const data = [
  {
    label: 'Tại nhà',
    value: 0,
  },
  {
    label: 'Tại cửa hàng',
    value: 1,
  },
];

const CartShopping = () => {
  const dispatch = useDispatch();

  const products = useSelector(state => state.cart.products);
  const [checked, setChecked] = useState('');
  const removeItemsFromCart = items => dispatch(removeFromCart(items));

  const handleRemoveItemsFromCart = items => {
    removeItemsFromCart(items);
  };

  function currencyFormatPayment(num) {
    return num.toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }

  return (
    <MView style={styles.container}>
      <MScrollView style={styles.container}>
        <MHeader
          onPress={() => goBack()}
          iconLeft={'arrow-left'}
          deleteIcon
          title={'labelScreen.CartShopping'}
        />
        <MView style={styles.content}>
          <MText
            style={styles.textLabel}
            text={'textLine.receiverInformation'}
          />
          <MView style={styles.rowView}>
            <MView style={styles.textInforView}>
              <IconF
                name="user-circle"
                size={22}
                color={colors.placeholderTextColor}
              />
              <MText
                style={styles.textInformation}
                numberOfLines={2}
                text={'Nguyễn Thành Thái'}
              />
            </MView>
            <MView style={styles.textInforView}>
              <IconM
                name="smartphone"
                size={22}
                color={colors.placeholderTextColor}
              />
              <MText
                style={styles.textInformation}
                numberOfLines={2}
                text={'0346826600'}
              />
            </MView>
          </MView>
          <MText style={styles.textLabel} text={'textLine.deliveryForm'} />
          <RadioButtonRN
            data={data}
            style={{flexDirection: 'row', justifyContent: 'space-between'}}
            boxStyle={{width: setHorizontal(screenW / 2 - 30)}}
            textStyle={{marginLeft: setHorizontal(20)}}
            initial={1}
            selectedBtn={checked => setChecked(checked)}
            icon={<IconF name="check-circle" size={25} color="#2c9dd1" />}
          />
          {checked.value == 0 ? (
            <MView style={{alignItems: 'center'}}>
              <MTextInput
                style={styles.inputAddress}
                numberOfLines={3}
                placeholder={'Vui lòng nhập địa chỉ chi tiết'}
              />
            </MView>
          ) : null}
          {checked.value == 1 ? (
            <MView style={{alignItems: 'center'}}>
              <MTextInput
                style={styles.inputAddress}
                editable={false}
                selectTextOnFocus={false}
                numberOfLines={3}
                placeholder={'Chọn cửa hàng'}
              />
            </MView>
          ) : null}
          <MView style={{alignItems: 'center'}}>
            <MButton style={styles.voucher}>
              <MView style={{alignItems: 'center', flexDirection: 'row'}}>
                <IconM
                  name="card-giftcard"
                  size={25}
                  color={colors.colors.colorBtnCounters}
                  style={{marginLeft: setHorizontal(10)}}
                />
                <MText style={styles.textVoucher} text={'Bạn có mă giảm giá'} />
              </MView>
              <IconM
                name="keyboard-arrow-right"
                size={25}
                color={colors.colors.colorBtnCounters}
                style={{marginRight: setHorizontal(10)}}
              />
            </MButton>
          </MView>
          <MView style={{alignItems: 'center'}}>
            <MTextInput
              style={styles.inputAddress}
              placeholder={'Thêm ghi chú'}
            />
          </MView>
          <MText style={styles.textLabel} text={'textLine.informationOrder'} />
          {products.map(data => {
            return (
              <MView
                style={[styles.rowView, styles.viewInformationOrder]}
                key={data.id}>
                <MView style={{flexDirection: 'row', alignItems: 'center'}}>
                  <MImage style={styles.imgItems} uri={data.url} />
                  <MView>
                    <MText style={styles.textInfor} text={data.title} />
                    <MText
                      style={styles.textInfor}
                      text={currencyFormatPayment(data.price)}
                    />
                  </MView>
                </MView>
                <MView style={{alignItems: 'center'}}>
                  {/* <IconFA
                    name="close"
                    size={20}
                    color={'red'}
                    onPress={() => handleRemoveItemsFromCart(data)}
                    style={{marginRight: -60, marginBottom: 5}}
                  /> */}
                  <MView
                    style={[
                      styles.rowView,
                      {borderWidth: 1, width: 80, borderRadius: 8},
                    ]}>
                    <MButtonIcon
                      containerStyle={{
                        backgroundColor: colors.colors.colorBtnCounters,
                        borderBottomLeftRadius: 8,
                        borderTopLeftRadius: 8,
                      }}
                      iconName={'add'}
                      iconSize={20}
                      iconColor={colors.iconColor.iconClose}
                    />
                    <MText
                      style={{
                        fontSize: setFontSize(13),
                        fontWeight: '500',
                        color: colors.textColors.primary,
                      }}
                      text={'1'}
                    />
                    <MButtonIcon
                      iconName={'remove'}
                      containerStyle={{
                        backgroundColor: 'red',
                        borderBottomRightRadius: 8,
                        borderTopRightRadius: 8,
                      }}
                      iconSize={20}
                      iconColor={colors.iconColor.iconClose}
                    />
                  </MView>
                </MView>
              </MView>
            );
          })}
        </MView>
      </MScrollView>
      <MView style={{alignItems: 'center'}}>
        <MButton
          style={{
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: colors.colors.colorBtnCounters,
            width: screenW,
            height: 70,
            marginTop: setVertical(30),
          }}>
          <MText style={styles.submit} text={'Mua hàng'} />
        </MButton>
      </MView>
    </MView>
  );
};

export default CartShopping;
