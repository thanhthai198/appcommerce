import {StyleSheet} from 'react-native';
import colors from '@theme/colors';
import {setVertical, setFontSize, setHorizontal, screenW} from '@utils/index';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(255, 255, 255, 0.3)',
  },
  content: {
    paddingHorizontal: setHorizontal(14),
  },
  textLabel: {
    width: setHorizontal(182),
    height: setVertical(20),
    fontSize: setFontSize(15),
    fontWeight: '600',
    color: colors.textColors.textLabel,
    marginTop: setVertical(11),
  },
  rowView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  textInforView: {
    width: setHorizontal(168),
    height: setVertical(45),
    backgroundColor: colors.colors.primary,
    borderRadius: setVertical(10),
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: setHorizontal(7),
  },
  textInformation: {
    color: colors.textColors.primary,
    fontSize: setFontSize(15),
    fontWeight: '500',
    marginLeft: setHorizontal(10),
  },
  inputAddress: {
    width: setHorizontal(screenW - 40),
    backgroundColor: 'rgba(255, 255, 255, 1)',
    minHeight: setVertical(40),
    borderRadius: setVertical(10),
    marginTop: setVertical(15),
  },
  voucher: {
    width: setHorizontal(screenW - 40),
    backgroundColor: 'white',
    height: setVertical(50),
    borderRadius: setVertical(10),
    marginTop: setVertical(12),
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  textVoucher: {
    color: colors.colors.colorBtnCounters,
    fontSize: setFontSize(15),
    fontWeight: '700',
    marginLeft: setHorizontal(10),
  },
  viewInformationOrder: {
    backgroundColor: colors.colors.primary,
    minHeight: setVertical(50),
    borderRadius: setVertical(10),
    marginTop: setVertical(7),
  },
  imgItems: {
    width: setVertical(50),
    height: setVertical(50),
    borderRadius: setVertical(10),
  },
  textInfor: {
    fontSize: setFontSize(13),
    fontWeight: '500',
    color: colors.textColors.primary,
    marginLeft: setVertical(15),
    marginVertical: setVertical(4),
  },
  submit: {
    fontSize: setFontSize(19),
    fontWeight: 'bold',
    color: colors.textColors.secondary,
  },
});
