import React, {useState, useEffect, useCallback} from 'react';
import {
  MText,
  MView,
  MHeader,
  BaseContainer,
  MFlatList,
  MButtonAddToCart,
  MButtonWText,
  MButton,
  MButtonIcon,
  MScrollView,
} from '@components/index';
import MImage from '@components/MImage';
import styles from './styles';
import colors from '@theme/colors';
import Modal from 'react-native-modal';
import IconF from 'react-native-vector-icons/FontAwesome';
import {
  addToCart,
  removeFromCart,
  subtractQuantity,
  addQuantity,
  emptyCart,
} from '@redux/actions/carShoppingAction';
import {useSelector, useDispatch} from 'react-redux';
import {navigate} from '@services/NavigationService';
import {SCREEN_NAMES} from '@constants/screenNames';

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'Quả cam',
    url: 'https://media.vov.vn/uploaded/69lwz2nmezg/2019_08_07/5_xgnl.jpg',
    price: 12000,
    compare: '30',
    mass: '1',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Quả quýt',
    url: 'https://cdn-www.vinid.net/4320c372-loi-ich-cua-qua-quyt-2.jpg',
    price: 15000,
    compare: '30',
    mass: '1',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Quả bưởi',
    url: 'https://cafefcdn.com/thumb_w/650/203337114487263232/2021/9/15/photo1631710626805-16317106270301338640448.jpg',
    price: 14000,
    compare: '60',
    mass: '1',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d73',
    title: 'Quả mít',
    url: 'https://tieudung.vn/upload_images/images/2021/04/07/mit.jpg',
    price: 13000,
    compare: '150',
    mass: '1',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d78',
    title: 'Quả sầu riêng',
    url: 'https://vinmec-prod.s3.amazonaws.com/images/20210308_062131_651831_an-sau-rieng.max-1800x1800.jpg',
    price: 17000,
    compare: '120',
    mass: '1',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d13',
    title: 'Quả roi',
    url: 'https://cheesewerks.com/wp-content/uploads/2020/07/B%C3%A0-b%E1%BA%A7u-%C4%83n-qu%E1%BA%A3-roi-c%C3%B3-t%E1%BB%91t-kh%C3%B4ng.jpg',
    price: 18000,
    compare: '90',
    mass: '1',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d23',
    title: 'Quả mận',
    url: 'https://media.cooky.vn/article/s640/Article2062-636009998149171923.jpg',
    price: 11000,
    compare: '60',
    mass: '1',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d33',
    title: 'Quả mơ',
    url: 'https://baithuocquy.com/img/upload/Article/2015/6/3nzzzzyj222223mwhbdp-qua-mo.jpg',
    price: 10000,
    compare: '300',
    mass: '1',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d43',
    title: 'Quả thanh long',
    url: 'https://cdn.youmed.vn/tin-tuc/wp-content/uploads/2020/05/thanh-long-1-1.jpg',
    price: '9000',
    compare: '600',
    mass: '1',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d53',
    title: 'Quả hồng',
    url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSul0ylwbtkCkxvE5OW8gCdHgtSAaf56uc1FQ&usqp=CAU',
    price: 20000,
    compare: '150',
    mass: '1',
  },
];

const MenuScreen = () => {
  const dispatch = useDispatch();
  const addItemsToCart = items => dispatch(addToCart(items));
  const removeItemsFromCart = items => dispatch(removeFromCart(items));
  const products = useSelector(state => state.cart.products);
  const [itemId, setItemId] = useState([]);

  const handleAddItemsToCart = items => {
    addItemsToCart(items);
    itemId.push(items.id);
  };

  const handleRemoveItemsFromCart = items => {
    removeItemsFromCart(items);
    const selectedId = items.id;
    const newData = itemId.filter(id => {
      return id !== selectedId;
    });
    setItemId(newData);
  };

  const viewBtnCartShopping = items => {
    if (itemId.includes(items.id)) {
      return (
        <MButtonAddToCart
          onPress={() => handleRemoveItemsFromCart(items)}
          title={'titleButton.removeFromCard'}
          iconName={'cart-outline'}
          iconSize={20}
          isBackgroundColor
          iconColor={colors.iconColor.primary}
        />
      );
    } else {
      return (
        <MButtonAddToCart
          onPress={() => handleAddItemsToCart(items)}
          title={'titleButton.addToCard'}
          iconName={'cart-outline'}
          iconSize={20}
          iconColor={colors.iconColor.primary}
        />
      );
    }
  };

  const [isModalItems, setModalItems] = useState(false);
  const [itemDetail, setItemDetail] = useState([]);
  const [wedgePrice, setWedgePrice] = useState(0);
  const [membershipPrice, setMembershipPrice] = useState(0);
  const [priceHasDropped, setPriceHasDropped] = useState(0);
  const [disabledLeft, setDisabledLeft] = useState(true);
  const [disabledRight, setDisabledRight] = useState(true);
  const [counters, setCounters] = useState(0);
  const [maxCounter, setMaxCounter] = useState(0);
  const [state, setState] = useState({
    radioBtn: [
      {
        id: '01',
        buyNow: 'Mua ngay',
        mass: '1',
        selected: false,
        save: '0',
      },
      {
        id: '02',
        buyNow: 'Mua ngay',
        selected: false,
        mass: '2',
        save: '5',
      },
      {
        id: '03',
        buyNow: 'Mua ngay',
        mass: '5',
        selected: false,
        save: '10',
      },
      {
        id: '04',
        buyNow: 'Mua ngay',
        mass: '10',
        selected: false,
        save: '20',
      },
      {
        id: '05',
        buyNow: 'Mua ngay',
        mass: '30',
        save: '35',
        selected: false,
      },
    ],
  });

  const toggleModal = data => {
    setModalItems(!isModalItems);
    setItemDetail(data);
  };

  function currencyFormatPayment(num) {
    return num.toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }

  function currencyFormatNormal(num) {
    return num.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }

  const _renderShowItemsDetail = () => {
    return (
      <>
        <MView style={styles.modalContent}>
          <MImage style={styles.backgroundItem} uri={itemDetail.url}>
            <MView style={styles.viewBtnClose}>
              <MButtonIcon
                iconName={'close-circle-outline'}
                iconSize={32}
                iconColor={colors.iconColor.iconClose}
                onPress={() => setModalItems(!isModalItems)}
              />
            </MView>
          </MImage>
          <MView style={styles.viewInfoItem}>
            <MText style={styles.textInfoItem} text={itemDetail.title} />
            <MView style={styles.rowText}>
              <MText style={styles.textMixedRace} text={'textLine.mixedRace'} />
              <MText
                style={styles.textMixedRace}
                text={itemDetail.compare + 'kg'}
              />
            </MView>
          </MView>
          <MView style={styles.viewRadioBtn}>
            <MScrollView horizontal={true} style={styles.scrollRadioBtn}>
              {state.radioBtn.map((data, index) => {
                const _Discount =
                  (data.save / 100) * (itemDetail.price * data.mass);
                const maxCompare = itemDetail.compare / data.mass;
                return (
                  <MButton
                    key={index}
                    onPress={() => {
                      const items = state.radioBtn;
                      if (
                        items.some(data => {
                          if (data.selected) return true;
                        })
                      ) {
                        const selectedIndex = items.findIndex(
                          data => data.selected === true,
                        );
                        if (selectedIndex == index) {
                          items[selectedIndex].selected = false;
                        } else {
                          items[selectedIndex].selected = false;
                          items[index].selected = !items[index].selected;
                        }
                      } else {
                        items[index].selected = !items[index].selected;
                      }
                      setState({radioBtn: items});
                      if (data.selected == true) {
                        setWedgePrice(itemDetail.price * data.mass);
                        setMembershipPrice(
                          (itemDetail.price * data.mass - _Discount) * 0.05,
                        );
                        setPriceHasDropped(
                          itemDetail.price * data.mass - _Discount,
                        );
                        setDisabledRight(false);
                        setDisabledLeft(false);
                        setCounters(1);
                        setMaxCounter(maxCompare);
                      } else {
                        setWedgePrice(0);
                        setMembershipPrice(0);
                        setPriceHasDropped(0);
                        setDisabledRight(true);
                        setDisabledLeft(true);
                        setCounters(0);
                      }
                    }}
                    style={
                      data.selected === true
                        ? styles.btnRadioBtnActive
                        : styles.btnRadioBtn
                    }>
                    <MView style={styles.viewCircles}>
                      <IconF
                        name={
                          data.selected === true
                            ? 'check-circle-o'
                            : 'circle-thin'
                        }
                        size={14}
                        color={
                          data.selected === true
                            ? colors.borderColors.iconActive
                            : colors.borderColors.primary
                        }
                        style={styles.iconCircles}
                      />
                    </MView>
                    <MView style={styles.viewTextRadioBtn}>
                      <MText
                        numberOfLines={3}
                        style={styles.textRadioBtn}
                        text={data.buyNow + ' ' + data.mass + 'kg'}
                      />
                    </MView>
                    <MView style={styles.viewPriceRadioBtn}>
                      <MText
                        style={styles.textPriceRadioBtn}
                        text={currencyFormatNormal(
                          `${itemDetail.price * data.mass - _Discount}`,
                        )}
                      />
                      <MText
                        style={styles.textPriceDiscount}
                        text={currencyFormatNormal(
                          `${
                            data.save === '0'
                              ? ''
                              : itemDetail.price * data.mass
                          }`,
                        )}
                      />
                    </MView>
                  </MButton>
                );
              })}
            </MScrollView>
          </MView>
          <MView style={styles.viewLabelBegin}>
            <MView style={[styles.rowText, {marginTop: 8}]}>
              <MText
                style={styles.textLabelBegin}
                text={'textLine.wedgePrice'}
              />
              <MText
                style={styles.textContent}
                text={currencyFormatPayment(wedgePrice * counters) + 'đ'}
              />
            </MView>
            <MView style={[styles.rowText, {marginTop: 8}]}>
              <MText
                style={styles.textLabelBegin}
                text={'textLine.priceHasDropped'}
              />
              <MText
                style={styles.textContent}
                text={currencyFormatPayment(priceHasDropped * counters) + 'đ'}
              />
            </MView>
            <MView style={[styles.rowText, {marginTop: 8}]}>
              <MText
                style={styles.textLabelBegin}
                text={'textLine.membershipPrice'}
              />
              <MText
                style={styles.textContent}
                text={currencyFormatPayment(membershipPrice * counters) + 'đ'}
              />
            </MView>
            <MView style={[styles.rowText, {marginTop: 8}]}>
              <MText
                style={styles.textLabelBegin}
                text={'textLine.lastPrice'}
              />
              <MText
                style={styles.textContent}
                text={
                  currencyFormatPayment(
                    priceHasDropped * counters - membershipPrice * counters,
                  ) + 'đ'
                }
              />
            </MView>
            <MView style={[styles.rowText, {marginTop: 20}]}>
              <MText
                style={styles.textLabelBegin}
                text={'textLine.quantityPurchased'}
              />
              <MView style={styles.quantity}>
                <MButton
                  disabled={disabledLeft}
                  onPress={() => {
                    if (counters <= 1) {
                      setCounters(1);
                    } else {
                      setCounters(counters - 1);
                    }
                  }}
                  style={
                    disabledLeft === true
                      ? styles.btnCounterLeftDisabled
                      : styles.btnCounterLeft
                  }>
                  <IconF name="minus" size={18} color={colors.colors.primary} />
                </MButton>
                <MText
                  style={styles.textCounter}
                  text={counters === 0 ? '0' : counters}
                />
                <MButton
                  disabled={disabledRight}
                  onPress={() => {
                    if (counters == maxCounter) {
                      return setCounters(maxCounter);
                    } else {
                      setCounters(counters + 1);
                    }
                  }}
                  style={
                    disabledRight === true
                      ? styles.btnCounterRightDisabled
                      : styles.btnCounterRight
                  }>
                  <IconF name="plus" size={18} color={colors.colors.primary} />
                </MButton>
              </MView>
            </MView>
            <MView style={styles.groundBtnSubmit}>
              <MButtonWText
                title="textLine.oderNow"
                style={styles.btnSubmitLeft}
              />
              <MButtonWText
                title="titleButton.addToCard"
                textStyle={styles.textSubmit}
                style={styles.btnSubmitRight}
              />
            </MView>
          </MView>
        </MView>
      </>
    );
  };

  return (
    <>
      <BaseContainer>
        <MHeader
          title={'labelScreen.Menu'}
          rightIcon={'shopping-cart'}
          textLengthCart={`${products.length}`}
          onPressRightButton={() => navigate(SCREEN_NAMES.CART_SHOPPING)}
        />
        <MView style={styles.container}>
          <MFlatList
            data={DATA}
            keyExtractor={item => item.id}
            numColumns={2}
            renderItem={({item}) => (
              <>
                <MButton onPress={() => toggleModal(item)} style={styles.items}>
                  <MImage style={styles.img} uri={item.url} />
                  <MText style={styles.titleName} text={item.title} />
                  <MView style={styles.rowText}>
                    <MText style={styles.titlePrice}>
                      {currencyFormatNormal(`${item.price}`) +
                        'đ/' +
                        item.mass +
                        'kg'}
                    </MText>
                  </MView>
                  {viewBtnCartShopping(item)}
                </MButton>
              </>
            )}
          />
        </MView>
      </BaseContainer>
      {/* <Modal
        animationType="none"
        transparent={true}
        style={styles.bottomModal}
        isVisible={isModalItems}>
        {_renderShowItemsDetail()}
      </Modal> */}
    </>
  );
};

export default MenuScreen;
