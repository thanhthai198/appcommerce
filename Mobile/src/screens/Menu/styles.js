import {StyleSheet} from 'react-native';
import colors from '@theme/colors';
import {
  setVertical,
  setFontSize,
  setHorizontal,
  screenW,
  screenH,
} from '@utils/index';

const heightItem = screenH * 0.35;
const withItem = screenW / 1.9 - 20;
const heightModalItem = screenH * 0.95;

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  items: {
    height: heightItem,
    backgroundColor: 'rgba(255,255,255,0.8)',
    marginVertical: setVertical(7),
    width: withItem,
    marginHorizontal: setHorizontal(5),
    borderRadius: setHorizontal(8),
    shadowColor: '#000',
    alignItems: 'center',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  img: {
    width: withItem,
    height: heightItem * 0.76,
  },
  titleName: {
    marginVertical: setVertical(3),
    fontSize: setFontSize(12),
    fontWeight: '600',
    marginHorizontal: setHorizontal(3),
  },
  titlePrice: {
    fontSize: setFontSize(12),
    fontWeight: '500',
    marginHorizontal: setHorizontal(3),
    color: colors.textColors.colorPrice,
  },
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalContent: {
    backgroundColor: colors.colors.primary,
    // padding: 10,
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    height: heightModalItem,
  },
  viewBtnClose: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    // marginTop: setVertical(20),
  },
  backgroundItem: {
    height: heightModalItem * 0.35,
  },
  textInfoItem: {
    fontSize: setFontSize(20),
    fontWeight: 'bold',
    color: colors.textColors.colorPrice,
  },
  viewInfoItem: {
    marginVertical: setVertical(10),
    paddingHorizontal: 10,
  },
  textMixedRace: {
    marginTop: setVertical(6),
    fontSize: setFontSize(13),
    color: colors.placeholderTextColor,
    fontWeight: '500',
    fontStyle: 'italic',
  },
  rowText: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  viewRadioBtn: {
    width: '100%',
    height: setVertical(143),
    marginTop: setVertical(15),
    paddingHorizontal: 10,
  },
  scrollRadioBtn: {
    backgroundColor: colors.colors.primary,
  },
  btnRadioBtn: {
    height: '100%',
    width: setHorizontal(97),
    borderRadius: setVertical(11),
    marginRight: setHorizontal(8),
    borderWidth: setHorizontal(1),
    borderColor: colors.borderColors.primary,
  },
  btnRadioBtnActive: {
    height: '100%',
    width: setHorizontal(97),
    borderRadius: setVertical(11),
    marginRight: setHorizontal(8),
    borderWidth: setHorizontal(1),
    borderColor: colors.borderColors.iconActive,
  },
  textRadioBtn: {
    fontWeight: '500',
    fontSize: setFontSize(14),
    width: setHorizontal(90),
    textAlign: 'center',
  },
  viewCircles: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginTop: setVertical(10),
    height: setVertical(15),
  },
  viewTextRadioBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    height: setVertical(80),
    marginTop: setVertical(3),
  },
  iconCircles: {
    marginRight: setHorizontal(7),
  },
  viewPriceRadioBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: setVertical(3),
    flexDirection: 'row',
  },
  textPriceRadioBtn: {
    fontWeight: '500',
    fontSize: setFontSize(12),
    textAlign: 'center',
    color: '#039447',
    marginHorizontal: setHorizontal(2.5),
  },
  textPriceDiscount: {
    fontWeight: '500',
    fontSize: setFontSize(10),
    textAlign: 'center',
    color: '#C10320',
    marginHorizontal: setHorizontal(2.5),
    textDecorationLine: 'line-through',
  },
  viewLabelBegin: {
    marginTop: setVertical(10),
    paddingHorizontal: 10,
  },
  textLabelBegin: {
    fontSize: setFontSize(14),
    fontWeight: '500',
    color: colors.textColors.primary,
  },
  textContent: {
    fontSize: setFontSize(12),
    color: colors.textColors.primary,
  },
  quantity: {
    height: setVertical(40),
    width: setHorizontal(140),
    marginHorizontal: setHorizontal(40),
    alignItems: 'center',
    flexDirection: 'row',
    borderWidth: setVertical(0.5),
    borderColor: colors.placeholderTextColor,
    borderRadius: setVertical(5),
  },
  textCounter: {
    width: setHorizontal(140 - 80),
    textAlign: 'center',
    fontSize: setFontSize(18),
    fontWeight: '600',
  },
  btnCounterLeft: {
    width: setHorizontal(40),
    height: setVertical(40),
    backgroundColor: colors.colors.colorBtnCounters,
    borderTopLeftRadius: setVertical(5),
    borderBottomLeftRadius: setVertical(5),
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnCounterLeftDisabled: {
    width: setHorizontal(40),
    height: setVertical(40),
    backgroundColor: colors.placeholderTextColor,
    borderTopLeftRadius: setVertical(5),
    borderBottomLeftRadius: setVertical(5),
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnCounterRight: {
    width: setHorizontal(40),
    height: setVertical(40),
    backgroundColor: colors.colors.colorBtnCounters,
    borderTopRightRadius: setVertical(5),
    borderBottomRightRadius: setVertical(5),
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnCounterRightDisabled: {
    width: setHorizontal(40),
    height: setVertical(40),
    backgroundColor: colors.placeholderTextColor,
    borderTopLeftRadius: setVertical(5),
    borderBottomLeftRadius: setVertical(5),
    alignItems: 'center',
    justifyContent: 'center',
  },
  groundBtnSubmit: {
    width: '100%',
    marginTop: setVertical(10),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 15,
  },
  btnSubmitLeft: {
    width: '48%',
    borderRadius: setVertical(8),
    backgroundColor: colors.colors.colorBtnCounters,
  },
  btnSubmitRight: {
    width: '48%',
    borderRadius: setVertical(8),
    backgroundColor: 'rgba(3, 148, 71, 0.12)',
  },
  textSubmit: {
    color: colors.colors.colorBtnCounters,
  },
});
