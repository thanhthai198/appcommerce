import {StyleSheet} from 'react-native';
import colors from '@theme/colors';
import {setVertical, setFontSize, setHorizontal} from '@utils/index';

export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
