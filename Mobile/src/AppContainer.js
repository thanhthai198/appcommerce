import RootNavigation from './navigation/RootNavigation';
import React, {useEffect} from 'react';
import {useSelector} from 'react-redux';
import {LoadingView} from './components';

function AppContainer() {
  const loading = useSelector(state => state.app.loading);

  return (
    <>
      <RootNavigation />
      {loading && <LoadingView />}
    </>
  );
}

export default AppContainer;
